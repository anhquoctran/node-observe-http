import { HttpHeaders } from '../headers';
import { Stream } from 'stream';

const toString = Object.prototype.toString;

export function isArray(arr: any) {
  return !!arr && Array.isArray(arr);
}

export function isUndefined(val: any) {
  return typeof val === 'undefined';
}

export function isBuffer(val: any) {
  return (
    val !== null &&
    !isUndefined(val) &&
    val.constructor !== null &&
    !isUndefined(val.constructor) &&
    typeof val.constructor.isBuffer === 'function' &&
    val.constructor.isBuffer(val)
  );
}

export function isArrayBuffer(val: any) {
  return toString.call(val) === '[object ArrayBuffer]';
}

export function isArrayBufferView(val: any) {
  let result = false;
  if (typeof ArrayBuffer !== 'undefined' && ArrayBuffer.isView) {
    result = ArrayBuffer.isView(val);
  } else {
    result = val && val.buffer && val.buffer instanceof ArrayBuffer;
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
export function isString(val: any): boolean {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
export function isNumber(val: any): boolean {
  return typeof val === 'number';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
export function isObject(val: any): boolean {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
export function isDate(val: any): boolean {
  return toString.call(val) === '[object Date]';
}

export function isStream(val: any): boolean {
  return val && val instanceof Stream;
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
export function isFunction(val: any): boolean {
  return toString.call(val) === '[object Function]';
}

export function trim(str: string) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

export function isAbsoluteURL(url: string) {
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
}

export function combineUrl(baseUrl: string, relativeUrl: string) {
  return relativeUrl
    ? baseUrl.replace(/\/+$/, '') + '/' + relativeUrl.replace(/^\/+/, '')
    : baseUrl;
}

export function isValidXss(requestURL: string) {
  var xssRegex = /(\b)(on\w+)=|javascript|(<\s*)(\/*)script/gi;
  return xssRegex.test(requestURL);
}