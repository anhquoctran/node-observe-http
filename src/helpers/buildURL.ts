import { escape } from 'querystring';

function encode(val: string) {
  if(val && typeof val === 'string') {
    return escape(val)
    .replace(/%40/gi, '@')
    .replace(/%3A/gi, ':')
    .replace(/%24/g, '$')
    .replace(/%2C/gi, ',')
    .replace(/%20/g, '+')
    .replace(/%5B/gi, '[')
    .replace(/%5D/gi, ']');
  } else {
    return '';
  }
}

function buildURL(url: string, params: any, paramsSerializer: (params: any) => any) {
  if(!params) {
    return url;
  }

  let serializedParams;
  if(paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if(false) {

  }
}