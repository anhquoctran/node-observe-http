import { HttpInterceptor } from './interceptor';

export class InterceptorManager {

  private handlers: HttpInterceptor[] = [];

  constructor() {}

  public use(interceptor: HttpInterceptor): number {
    this.handlers.push(interceptor);
    return this.handlers.length - 1;
  }

  public eject(id: number) {
    if(this.handlers[id]) {
      this.handlers.splice(id, 1);
    }
  }

  public forEach(callback: (interceptor: HttpInterceptor) => void) {
    for(let interceptor of this.handlers) {
      if(interceptor) {
        callback(interceptor);
      }
    }
  }
}