import { InterceptorManager } from './interceptor-manager'

export class HttpClient {
  constructor() {}

  private _interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager(),
  }

  public get interceptors() {
    return this._interceptors;
  }
}