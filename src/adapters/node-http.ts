import * as http from 'http';
import * as https from 'https';
import * as url from 'url';
import { stringify } from 'querystring';

import { Observable, Observer } from 'rxjs';
import { HttpBackend } from '../backend';
import { HttpHeaders } from '../headers';
import { HttpRequest } from '../request';
import {
  HttpDownloadProgressEvent,
  HttpErrorResponse,
  HttpEvent,
  HttpEventType,
  HttpHeaderResponse,
  HttpJsonParseError,
  HttpResponse,
  HttpUploadProgressEvent
} from '../response';
import *  as pkg from '../../package.json';
import { isArrayBuffer, isString, isStream } from '../helpers/util';

const XSSI_PREFIX = /^\)\]\}',?\n/;
const isHttps = /https:?/;

interface PartialResponse {
  headers: HttpHeaders;
  status: number;
  statusText: string;
  url: string;
}

export interface HttpRequestData {
  params?: { [key: string]: string | string[] };
  body?: any;
  authentication?: { username: string, password: string };
}

export class NodeHttpAdapter {

  private readonly requestOptions: http.RequestOptions | https.RequestOptions;
  private readonly useHttps: boolean = false;

  constructor(options: http.RequestOptions | https.RequestOptions, useHttps: boolean = false) {
    this.requestOptions = options;
    this.useHttps = useHttps;
  }

  buildRequest(data: HttpRequestData, callback?: (error: Error, data: any) => void): void {
    const userAgent = this.requestOptions.headers['User-Agent'] || this.requestOptions.headers['user-agent'];
    if(!userAgent) {
      this.requestOptions.headers['User-Agent'] = 'obrequest/' + pkg.version;
    }
    let request: http.ClientRequest;
    if(this.useHttps) {
      request = https.request(this.requestOptions as https.RequestOptions, (res: http.IncomingMessage) => {
        res.setEncoding('utf8');
        res.on('data', (chunk) => {
          if(callback && typeof callback === 'function') {
            callback(undefined, chunk);
          }
        })
      });
    } else {
      request = http.request(this.requestOptions as http.RequestOptions, (res: http.IncomingMessage) => {
        res.setEncoding('utf8');
        res.on('data', (chunk) => {
          if(callback && typeof callback === 'function') {
            callback(undefined, chunk);
          }
        })
      });
    }

    if (data && data.body && !isStream(data.body)) {
      if (Buffer.isBuffer(data)) {
        // Nothing to do...
      } else if (isArrayBuffer(data)) {
        data.body = Buffer.from(new Uint8Array(data.body));
      } else if (isString(data)) {
        data.body = Buffer.from(data.body, 'utf-8');
      } else {
        
      }

      // Add Content-Length header if data exists
      this.requestOptions.headers['Content-Length'] = data.body.length;
    }
    let auth = '';
    if(data && data.authentication) {
      const username = data.authentication.username || '';
      const password = data.authentication.password || '';
      auth = `${username}:${password}`;
    }

    request.on('error', (err) => {
      if(callback && typeof callback === 'function') {
        callback(err, undefined);
      }
    })
    request.end();
  }
}
